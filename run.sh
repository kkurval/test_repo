#!/bin/bash
cd /opt/labs/
git clone https://bitbucket.org/kkurval/python_v.git
mv /opt/labs/test_repo/REST_API_checks_3_4_7 /opt/labs/mc18-checks/REST_API_checks_3_4_7
cd /opt/labs/mc18-checks
source /opt/labs/python_v/env/bin/activate
python REST_API_checks_3_4_7
